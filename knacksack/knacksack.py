import numpy as np

def aux(item):
    print(item)
    return  (item[0],item[1])


path = get_ipython().getoutput('pwd')
with open('{path}/mochila02.txt'.format(path= path[0])) as file:
    data = np.array([i.split() for i in file.read().split("\n")[:-1]]).transpose().astype("int")
    values = data[1]
    weigths = data[0]
    print(values,weigths)

maxWeigth, n = values[0], weigths[0]
memoization = [[None for x in range(maxWeigth+1)] for x in range(n+1)]


def knapsack(maxWeigth, n, values, weigths):
    for i in range(n+1): 
        for j in range(maxWeigth+1): 
            if i==0 or j==0: 
                memoization[i][j] = 0
            elif weigths[i] <= j:                                   
                withItem = values[i] + memoization[i-1][j-weigths[i]]
                withoutItem = memoization[i-1][j]
                memoization[i][j] = withItem if withItem > withoutItem else withoutItem
            else:
                memoization[i][j] = memoization[i-1][j]
    return memoization[n][maxWeigth]



memoized = knapsack(maxWeigth,n,values,weigths)

def findPickedItems(i, currentWeigth):
    pickedItems = []
    while i>0 and currentWeigth>0:
        heavier =  memoization[i][currentWeigth] != memoization[i-1][currentWeigth]
        if heavier:
            pickedItems.append(i)
            currentWeigth -= weigths[i]
        i -= 1
            
    return pickedItems

pickedItems = findPickedItems(n, maxWeigth)

        
        